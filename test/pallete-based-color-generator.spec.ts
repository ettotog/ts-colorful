import { expect } from 'chai';
import { PaletteBasedColorGenerator } from '../src/generator';
import { PaletteFactory, NEBULAR_COSMIC } from '../src/palette';


describe('PaletteBasedColorGenerator', () => {
    it('new PaletteBasedColorGenerator', () => {
        new PaletteBasedColorGenerator([]);
    })
    it('next', () => {
        const paletteFactory = new PaletteFactory();
        const palette = paletteFactory.createFromArray(NEBULAR_COSMIC);
        const colorGenerator = new PaletteBasedColorGenerator(palette, 0.02);
        for (let i = 0; i < palette.length; i += 1) {
            const currentColor = colorGenerator.next();
            expect(currentColor.toRgbaFunc()).to.equal(palette[i].toRgbaFunc());
        }
        for (let i = 0; i < palette.length; i += 1) {
            const currentColor = colorGenerator.next();
            expect(currentColor.rgbDistance(palette[i]) < 0.05).to.be.true;
        }
        for (let i = 0; i < palette.length; i += 1) {
            const currentColor = colorGenerator.next();
            expect(currentColor.rgbDistance(palette[i]) < 0.05).to.be.true;
        }
    })
})