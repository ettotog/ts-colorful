# OpenCIAg | ciag-colorful

[![Build Status](https://travis-ci.org/OpenCIAg/colorful.svg?branch=master)](https://travis-ci.org/OpenCIAg/colorful)
[![Maintainability](https://api.codeclimate.com/v1/badges/acab5c036cce9af7f29f/maintainability)](https://codeclimate.com/github/OpenCIAg/colorful/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/acab5c036cce9af7f29f/test_coverage)](https://codeclimate.com/github/OpenCIAg/colorful/test_coverage)
[![npm version](https://badge.fury.io/js/ciag-colorful.svg)](https://badge.fury.io/js/ciag-colorful)

## Install

```shell
npm install @ettotog/colorful
```

## Usage example with Angular

### Add the service

```ts
import { NgModule } from '@angular/core';
import { ColorService } from '@ettotog/colorful';
import { PaletteBasedColorGenerator } from '@ettotog/colorful/generator';
import { PaletteFactory, NEBULAR_CORPORATE } from '@ettotog/colorful/palette';

const paletteFactory = new PaletteFactory();
const palette = paletteFactory.createFromArray(NEBULAR_CORPORATE);

@NgModule({
    providers: [{
        provide: ColorService,
        useValue: new ColorService(new PaletteBasedColorGenerator(palette,0.3))
    ]}
})
export class AppModule{}
```

### Use the service

```ts
import { Component } from '@angular/core';
import { ColorService } from '@ettotog/colorful';

@Component({
    selector: 'colorful-component',
    templateUri: './colorful-component.html',
})
export class ColorfulComponent {

    constructor(
        private colorService: ColorService,
    ) {
        this.colorService.createColor();
        //create a new color
        this.colorService.createColor();

        this.colorSerice.createColor('unique');
        //create the same color
        this.colorSerice.createColor('unique');
    }

}

```
