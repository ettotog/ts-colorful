import { Color } from '..';
import { RGBAColor } from '../color';

export type RawPalette = string;
export type RawNamedPalette = { [name: string]: string };
export type Palette = Color[];
export type NamedPalette = { [name: string]: Color }



export const NEBULAR_COSMIC = {
    primary: '#7659FF',
    warning: '#FFA100',
    success: '#00D977',
    info: '#0088FF',
    danger: '#FF386A',
}

export const NEBULAR_LIGHT = {
    primary: '#8A7FFF',
    warning: '#FFA100',
    success: '#40DC7E',
    info: '#4CA6FF',
    danger: '#FF4C6A',
}

export const NEBULAR_CORPORATE = {
    primary: '#73A1FF',
    warning: '#FFA36B',
    success: '#5DCFE3',
    info: '#BA7FEC',
    danger: '#FF6B83',
}

export class PaletteFactory {

    createFromObject(rawNamedPalette: RawNamedPalette): NamedPalette {
        return Object.entries(rawNamedPalette)
            .map(([k, v]) => [k, RGBAColor.fromHexString(v)] as [string, Color])
            .reduce((a, [k, v]) => Object.assign(a, { [k]: v }), {})
    }

    createFromArray(rawPalette: RawPalette | RawNamedPalette): Palette {
        return Object.values(rawPalette)
            .map(it => RGBAColor.fromHexString(it));
    }


}