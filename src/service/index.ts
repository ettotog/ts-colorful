import { Color } from '../color';
import { ColorGenerator } from '../generator';
import { ColorStorage } from '../storage';

export class ColorService {

    constructor(
        private storage: ColorStorage,
        private colorGenerator: ColorGenerator
    ) {

    }

    public createColor(resource: string = null): Color {
        if (resource === null) {
            return this.colorGenerator.next();
        }
        if (this.storage.contains(resource)) {
            return this.storage.get(resource);
        }
        const color = this.colorGenerator.next();
        this.storage.set(resource, color);
        return color;
    }


}