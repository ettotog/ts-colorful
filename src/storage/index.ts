import { Color, RGBAColor } from "../color";


export class ColorStorage {

    constructor(
        private readonly storate: Storage
    ) {

    }

    set(key: string, value: Color) {
        this.storate.setItem(key, value.toHex());
    }

    get(key: string): Color {
        const hexColor = this.storate.getItem(key);
        return RGBAColor.fromHexString(hexColor);
    }

    remove(key: string): void {
        this.storate.removeItem(key);
    }

    contains(key: string): boolean {
        return !!this.storate.getItem(key);
    }

    static local(): ColorStorage {
        return new ColorStorage(window.localStorage);
    }

    static session(): ColorStorage {
        return new ColorStorage(window.sessionStorage);
    }

}